require_relative "spec_helper"

describe "gitlab-elk::default" do
  let(:chef_run) {
    ChefSpec::SoloRunner.new { |node|
      node.normal["gitlab-elk"] = {"logstash" => { "enabled" => true },
                                   "kibana" => { "enabled" => true }}
    }.converge(described_recipe)

  }

  it "installs logstash" do
    expect(chef_run).to install_package('logstash').with(version: '1:5.2.2-1')
  end

  it "installs kibana" do
    expect(chef_run).to install_package('kibana').with(version: '5.4.0')
  end
end
