name             'gitlab-elk'
maintainer       'GitLab Inc.'
maintainer_email 'jeroen@gitlab.com'
license          'MIT'
description      'Installs/Configures gitlab-elk'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.2.29'

depends 'gitlab-vault'
depends 'chef_nginx'
