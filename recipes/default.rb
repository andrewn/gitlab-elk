#
# Cookbook Name:: gitlab-elk
# Recipe:: default
#
# Copyright 2016, GitLab Inc.
#
# All rights reserved - Do Not Redistribute
#
include_recipe 'gitlab-vault'

# Fetch secrets from Chef Vault
elk_conf = GitLab::Vault.get(node, 'gitlab-elk')

package 'curl'
package 'openjdk-8-jre'

## Add apt repositories
file '/etc/apt/sources.list.d/elastic.list' do
  content "deb https://artifacts.elastic.co/packages/5.x/apt stable main\n"
  notifies :run, 'bash[add key]', :immediately
end

bash 'add key' do
  code <<-EOH
    curl -s https://artifacts.elastic.co/GPG-KEY-elasticsearch | apt-key add -
    apt-get update
  EOH
  action :nothing
end

if node['gitlab-elk']['logstash']['enabled']
  ## Install logstash
  package 'logstash' do
    action :install
  end

  package 'logstash' do
    version "#{node['gitlab-elk']['logstash']['version']}"
    action :lock
  end

  template '/etc/logstash/jvm.options' do
    owner 'root'
    group 'root'
    mode '0644'
    notifies :restart, 'service[logstash]'
  end

  service 'logstash' do
    action :enable
  end

  template '/etc/logstash/conf.d/gitlab' do
    source 'logstash-gitlab.erb'
    owner 'root'
    group 'root'
    mode '0644'
    notifies :restart, 'service[logstash]'
  end

  template '/etc/logstash/logstash.yml' do
    source 'logstash.yml.erb'
    owner 'root'
    group 'root'
    mode '0664'
    notifies :restart, 'service[logstash]'
  end

  group 'adm' do
    append true
    members ['logstash']
  end
end

if node['gitlab-elk']['kibana']['enabled']
  ## Install kibana
  package 'kibana' do
    action :install
  end

  package 'kibana' do
    version "#{node['gitlab-elk']['kibana']['version']}"
    action :lock
  end

  template '/etc/kibana/kibana.yml' do
    owner 'root'
    group 'root'
    mode '0644'
    notifies :restart, 'service[kibana]'
  end

  service 'kibana' do
    action :enable
  end

  ## Install nginx
  include_recipe 'chef_nginx'
  template "#{node['nginx']['dir']}/sites-available/kibana" do
    source 'nginx-site-kibana.erb'
    owner  'root'
    group  'root'
    mode   '0644'
    variables(
      :fqdn => elk_conf['kibana']['fqdn']
    )
    notifies :restart, resources(:service => 'nginx')
  end

  file "/etc/ssl/#{elk_conf['kibana']['fqdn']}.crt" do
    content elk_conf['kibana']['ssl_certificate']
    owner 'root'
    group 'root'
    mode 0644
    notifies :restart, resources(:service => 'nginx'), :delayed
  end

  file "/etc/ssl/#{elk_conf['kibana']['fqdn']}.key" do
    content elk_conf['kibana']['ssl_key']
    owner 'root'
    group 'root'
    mode 0600
    notifies :restart, resources(:service => 'nginx'), :delayed
  end

  nginx_site 'default' do
    enable false
  end

  nginx_site 'kibana' do
    enable true
  end

end
